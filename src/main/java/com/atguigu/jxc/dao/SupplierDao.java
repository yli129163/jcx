package com.atguigu.jxc.dao;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SupplierDao {


    List<Supplier> list(@Param("page") Integer page, @Param("rows") Integer rowsList, @Param("supplierName") String supplierName) ;

    Integer getSupplier(@Param(value = "supplierName") String supplierName);

    void saveSupplier(Supplier supplier);

    void setSupper( @Param(value = "supplier") Supplier supplier,@Param(value = "supplierId") Integer supplierId);

    void deleteSupplier(Integer supplierId);
}
