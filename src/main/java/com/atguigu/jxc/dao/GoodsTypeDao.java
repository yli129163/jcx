package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品类别
 */
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);


    void insertGoodsType(@Param(value = "goodsTypeName") String goodsTypeName, @Param(value = "pId") Integer pId);


    GoodsType select(@Param(value = "goodsTypeId") Integer goodsTypeId);

    void delete(Integer goodsTypeId);

    void update(Integer pId);
}
