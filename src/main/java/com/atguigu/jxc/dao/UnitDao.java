package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 用户
 */
public interface UnitDao {


    List<Unit>  list();
}
