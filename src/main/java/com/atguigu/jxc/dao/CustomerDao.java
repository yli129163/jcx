package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description
 */
public interface CustomerDao {



    List<Character> list(@Param(value = "page") Integer page, @Param(value = "rows") Integer rows, @Param(value = "customerName") String customerName);

    void saveCustomer(Customer customer);

    void setCustomer(@Param(value = "customer") Customer customer,@Param(value = "customerId") Integer customerId);

    void deleteCustomer(Integer supplierId);
}
