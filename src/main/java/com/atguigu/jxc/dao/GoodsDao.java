package com.atguigu.jxc.dao;



import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();

  /*  Integer getGoodsInventoryCount(@Param("codeOrName") String codeOrName, @Param("goodsTypeId") Integer goodsTypeId);*/

    List<Goods> getGoodsInventoryList(@Param("offSet") Integer offSet, @Param("pageRow") Integer pageRow, @Param("codeOrName") String codeOrName, @Param("goodsTypeId") Integer goodsTypeId);

    void insert(Goods goods);

    void update(@Param(value = "goods") Goods goods, @Param(value = "goodsId") Integer goodsId);

    

    void deleteGoodsID(Integer goodsId);

    Goods selectGoodsId(Integer goodsId);

    List<Goods> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode);
}
