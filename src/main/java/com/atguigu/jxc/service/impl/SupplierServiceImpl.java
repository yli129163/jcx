package com.atguigu.jxc.service.impl;


import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierDao supplierDao;

    @Override
    public Map<String, Object> list(Integer page, Integer rows, String supplierName) {
        List<Supplier> suppliers = supplierDao.list(page,rows ,supplierName);

        Integer totalCount = supplierDao.getSupplier(supplierName);

        int startIndex = (page - 1) *  rows;
        int endIndex = Math.min(startIndex + rows, suppliers.size()); // 计算结束索引(防止越界)

        List<Supplier> paginatedList = suppliers.subList(startIndex, endIndex); // 截取分页

        Map<String, Object> map = new HashMap<>();

        map.put("total",totalCount);
        map.put("rows" ,  paginatedList);

        return map;
    }

    @Override
    public void save(Supplier supplier, Integer supplierId) {
        if (supplierId == null) {
            // 如果供应商ID为空，则执行添加供应商的逻辑
            supplierDao.saveSupplier(supplier);
        } else {
            supplierDao.setSupper(supplier,supplierId);
        }


    }

    @Override
    public void delete(String ids) {
        String[] idArray = ids.split(",");
        List<Integer> supplierIds = new ArrayList<>();

        for (String id : idArray) {
            supplierIds.add(Integer.parseInt(id.trim()));
        }
        for (Integer supplierId : supplierIds) {
            supplierDao.deleteSupplier(supplierId);
        }
    }


}
