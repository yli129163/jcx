package com.atguigu.jxc.service.impl;


import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerDao customerDao;
    @Override
    public Map<String, Object> list(Integer page, Integer rows, String customerName) {

    List<Character> characterList = customerDao.list( page ,rows,customerName);


        Map<String, Object> map = new HashMap<>();

        map.put("total" , 3);
        map.put("rows",characterList);

        return map;

    }

    @Override
    public void save(Customer customer, Integer customerId) {

        if (customerId == null) {
            // 如果供应商ID为空，则执行添加供应商的逻辑
            customerDao.saveCustomer(customer);
        } else {
            customerDao.setCustomer(customer, customerId);
        }
    }

    @Override
    public void delete(String ids) {

        String[] idArray = ids.split(",");
        List<Integer> customerIds = new ArrayList<>();

        for (String id : idArray) {
            customerIds.add(Integer.parseInt(id.trim()));
        }
        for (Integer supplierId : customerIds) {
            customerDao.deleteCustomer(supplierId);
        }
    }
}
