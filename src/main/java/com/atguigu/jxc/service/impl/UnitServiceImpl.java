package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.RoleDao;
import com.atguigu.jxc.dao.UnitDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.dao.UserRoleDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.UnitService;
import com.atguigu.jxc.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
public class UnitServiceImpl implements UnitService {



    @Autowired
    private UnitDao unitDao;

    @Override
    public Map<String, Object> list() {
        List<Unit> unitList = unitDao.list();
        Map<String, Object> map = new HashMap<>();

        map.put("rows",unitList);
        return map;
    }
}
