package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

public interface SupplierService {
   Map<String, Object> list(Integer page, Integer rows, String supplierName);


   void save(Supplier supplier, Integer supplierId);


   void delete(String ids);
}
