package com.atguigu.jxc.controller;




import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RequestMapping("/customer")
@RestController
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @RequestMapping(value = "/list")
    public Map<String , Object> list(Integer page, Integer rows, String  customerName ){
        return customerService.list(page,rows,customerName);
    }

    @RequestMapping(value ="/save" )
    public ServiceVO save(Customer customer , Integer customerId) {
        customerService.save(customer, customerId);
        return new ServiceVO(100, "请求为空");
    }

    @RequestMapping(value ="/delete" )
    public ServiceVO delete(String ids){
        customerService.delete(ids);
        return new ServiceVO(100,"请求为空");
    }
}
