package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/supplier")
public class SupplierController {



    @Autowired
    private SupplierService supplierService;


    @RequestMapping("/list")
    public Map<String,Object> list(Integer page ,Integer rows ,String supplierName){
        return supplierService.list(page,rows,supplierName);
    }


    @RequestMapping("/save")
    public ServiceVO save( Supplier supplier , Integer supplierId){
        supplierService.save(supplier,supplierId);
        return new ServiceVO(100,"请求为空");
    }

    @RequestMapping("/delete")
    public ServiceVO delete(String ids){
        supplierService.delete(ids);
        return new ServiceVO(100,"请求为空");
    }


}
