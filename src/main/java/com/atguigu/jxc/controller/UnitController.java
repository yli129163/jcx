package com.atguigu.jxc.controller;

import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RequestMapping(value = "/unit")
@RestController
public class UnitController {
    @Autowired
    private UnitService unitService;

    @RequestMapping("/list")
    public Map<String , Object> list(){
        return unitService.list();
    }



}
